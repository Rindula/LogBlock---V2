package mc.doryan.fr.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.doryan.fr.Core;
import mc.doryan.fr.LogBlockAPI;

public class Commands implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}
		Player player = (Player) sender;
		if (player.hasPermission("logblock.commands")) {
			if (args.length == 0) {
				player.sendMessage("�7�m--------------------�7[ �cLogBlock �7]�7�m--------------------");
				player.sendMessage("");
				player.sendMessage("�cAuthor: �fRindula");
				player.sendMessage("�cVersion: �f" + Core.instance.getDescription().getVersion());
				if (Core.instance.getSpigotUpdater() == null) {
					player.sendMessage("�cUpdate: �athis plugin has update !");
				} else {
					player.sendMessage("�cUpdate: " + (!Core.instance.getSpigotUpdater().isUpdater().booleanValue()
							? "�athis plugin has update !"
							: "�cthis plugin does not have update !"));
				}
				player.sendMessage("�cDescription: �aAllows to have information on a block");
				player.sendMessage("");		
				player.sendMessage("�7List all commands:");
				player.sendMessage("�6- �e/lbr");
				player.sendMessage("�6- �e/lbr info");
				player.sendMessage("�6- �e/lbr wand");
				player.sendMessage("�6- �e/lbr debug");
				player.sendMessage("");
				player.sendMessage("�7�m----------------------------------------------------");
				return true;
			}
			if (args[0].equalsIgnoreCase("info")) {
				Core.instance.isBlockInfo.add(player);
				player.sendMessage(LogBlockAPI.getPrefix() + "�6Break a block to get block infomation !");
			}
			if (args[0].equalsIgnoreCase("wand")) {
				ItemStack wandStack = new ItemStack(Material.PAPER);
				ItemMeta wandMeta = wandStack.getItemMeta();
				wandMeta.setDisplayName("�6BlockInfo �7(�fright-click �7or �fleft-click�7)");
				wandStack.setItemMeta(wandMeta);

				player.getInventory().addItem(wandStack);
				player.sendMessage(LogBlockAPI.getPrefix() + "�6You have the wand for block info !");
			}
			if (args[0].equalsIgnoreCase("debug")) {
				LogBlockAPI.CONSOLE_DEBUG = !LogBlockAPI.CONSOLE_DEBUG; 
				player.sendMessage(LogBlockAPI.getPrefix() + "�6You have set debug : "+(LogBlockAPI.CONSOLE_DEBUG ? "�aYes" : "�CNo")+"�6.");
			}
		} else {
			player.sendMessage(LogBlockAPI.getPrefix() + "�cYou don't have permission !");
		}
		return false;
	}
}
