package mc.doryan.fr.listeners;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import mc.doryan.fr.Core;
import mc.doryan.fr.LogBlockAPI;
import mc.doryan.fr.objekt.BlockInfo;

public class Events implements Listener {
	@EventHandler
	public void PlayerJoinEvent_(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if ((p.isOp()) && (!Core.instance.getSpigotUpdater().isUpdater().booleanValue())) {
			p.sendMessage("�7�m----------------------------------------------------");
			p.sendMessage("");
			p.sendMessage(LogBlockAPI.getPrefix() + "�cThis plugin is not update !");
			p.sendMessage(LogBlockAPI.getPrefix() + "�cDownload here");
			p.sendMessage(LogBlockAPI.getPrefix() + "�ehttp://www.spigotmc.org/resources/32909");
			p.sendMessage("");
			p.sendMessage("�7�m----------------------------------------------------");
		}
	}

	@EventHandler
	public void BlockPlaceEvent_(BlockPlaceEvent e) {
		LogBlockAPI.addBlockInfo(e.getBlock(), e.getPlayer());
	}

	@EventHandler
	public void BlockBreakEvent_(BlockBreakEvent e) {
		Player player = e.getPlayer();
		ItemStack itemInHand = player.getInventory().getItemInMainHand();

		if (Core.instance.isBlockInfo.contains(e.getPlayer())
				|| (itemInHand.hasItemMeta() && itemInHand.getItemMeta().hasDisplayName() && itemInHand.getItemMeta()
						.getDisplayName().equalsIgnoreCase("�6BlockInfo �7(�fright-click �7or �fleft-click�7)"))) {
			BlockInfo blockInfo = LogBlockAPI.getBlockinfo(e.getBlock());
			LogBlockAPI.sendMessageInfoBlock(player, blockInfo);
			e.setCancelled(true);
		} else {
			LogBlockAPI.removeBlockInfo(e.getBlock());
		}
	}

	@EventHandler
	public void PlayerInterractEvent_(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Player player = e.getPlayer();
			ItemStack itemInHand = player.getInventory().getItemInMainHand();

			if (itemInHand.hasItemMeta() && itemInHand.getItemMeta().hasDisplayName() && itemInHand.getItemMeta()
					.getDisplayName().equalsIgnoreCase("�6BlockInfo �7(�fright-click �7or �fleft-click�7)")) {
				Block block = e.getClickedBlock();
				BlockInfo blockInfo = LogBlockAPI.getBlockinfo(block);
				LogBlockAPI.sendMessageInfoBlock(player, blockInfo);
			}
		}
	}
}
