package mc.doryan.fr;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import mc.doryan.fr.objekt.BlockInfo;

public class LogBlockAPI {

	public static boolean CONSOLE_DEBUG = false;

	public static void log(String message) {
		if (!(CONSOLE_DEBUG))
			return;
		Bukkit.getServer().getConsoleSender().sendMessage(getPrefix() + message);
	}

	public static BlockInfo getBlockinfo(Block block) {
		for (String objects : Core.getBlocksConfig().getConfigurationSection("blocks").getKeys(false)) {
			objects = "blocks." + objects;
			if (objects.contains("blocks.block")) {
				BlockInfo blockInfo = new BlockInfo(objects);

				LogBlockAPI.log(
						"�6blockX: �e" + block.getX() + "�7 / �6logBlockX: �e" + blockInfo.getLocation().getBlockX());
				LogBlockAPI.log(
						"�6blockY: �e" + block.getY() + "�7 / �6logBlockY: �e" + blockInfo.getLocation().getBlockY());
				LogBlockAPI.log(
						"�6blockZ: �e" + block.getZ() + "�7 / �6logBlockZ: �e" + blockInfo.getLocation().getBlockZ());

				if ((block.getLocation().getWorld().getName().equals(blockInfo.getLocation().getWorld().getName()))
						&& (block.getX() == blockInfo.getLocation().getBlockX())
						&& (block.getY() == blockInfo.getLocation().getBlockY())
						&& (block.getZ() == blockInfo.getLocation().getBlockZ())) {
					return blockInfo;
				}
			}
		}
		return null;
	}

	public static String getPrefix() {
		return "�7[�cLogBlock�7] ";
	}

	public static void sendMessageInfoBlock(Player player, BlockInfo blockInfo) {
		if (player == null)
			return;

		if (blockInfo == null) {
			player.sendMessage(getPrefix()
					+ "�cThis block is not in the file (this block was placed before inserting the plugin or this block was not placed by a player)!");
			return;
		}

		player.sendMessage("�7�m--------------------�7[ �cLogBlock �7]�7�m--------------------");
		player.sendMessage("");
		player.sendMessage("�7Block Type: �f" + blockInfo.getBlock().toString().replace("_", " "));
		player.sendMessage("�7Author: �f" + blockInfo.getAuthor());
		player.sendMessage("�7Date: �f" + blockInfo.getDate());
		player.sendMessage("�7GameMode: �f" + blockInfo.getGameMode().toString());
		player.sendMessage("�7Location: �e" + blockInfo.getLocation().getWorld().getName() + "�f, �e"
				+ blockInfo.getLocation().getX() + "�f, �e" + blockInfo.getLocation().getY() + "�f, �e"
				+ blockInfo.getLocation().getZ());
		player.sendMessage("");
		player.sendMessage("�7�m----------------------------------------------------");
		Core.instance.isBlockInfo.remove(player);
	}

	public static boolean removeBlockInfo(Block block) {
		BlockInfo blockInfo = getBlockinfo(block);

		if(blockInfo == null)return false;

		Core.getBlocksConfig().set("locations.block" + blockInfo.getID() + ".world", null);
		Core.getBlocksConfig().set("locations.block" +  blockInfo.getID() + ".x", null);
		Core.getBlocksConfig().set("locations.block" +  blockInfo.getID() + ".y", null);
		Core.getBlocksConfig().set("locations.block" +  blockInfo.getID() + ".z", null);
		Core.getBlocksConfig().set("blocks.block" +  blockInfo.getID() + ".author", null);
		Core.getBlocksConfig().set("blocks.block" +  blockInfo.getID() + ".block", null);
		Core.getBlocksConfig().set("blocks.block" + blockInfo.getID() + ".date", null);
		Core.getBlocksConfig().set("blocks.block" + blockInfo.getID() + ".gameMode", null);
		Core.getBlocksConfig().set("blocks.block" +  blockInfo.getID() + ".location.world", null);
		Core.getBlocksConfig().set("blocks.block" +  blockInfo.getID() + ".location.x", null);
		Core.getBlocksConfig().set("blocks.block" +  blockInfo.getID() + ".location.y", null);
		Core.getBlocksConfig().set("blocks.block" +  blockInfo.getID() + ".location.z", null);
		Core.getBlocksConfig().set("blocks.block" +  blockInfo.getID(), null);
		

		Core.getBlocksConfig().set("locations.block" + blockInfo.getID() + ".world", null);
		Core.getBlocksConfig().set("locations.block" + blockInfo.getID() + ".x", null);
		Core.getBlocksConfig().set("locations.block" + blockInfo.getID() + ".y", null);
		Core.getBlocksConfig().set("locations.block" + blockInfo.getID() + ".z", null);
		Core.getBlocksConfig().set("locations.block" + blockInfo.getID(), null);
		
		try {
			Core.getBlocksConfig().save(Core.getBlockFile());
			Core.log("�aBlock file is saved!");
			Core.log("�aBlock removed!");
			return true;
		} catch (IOException e1) {
			Core.log("�cBlock file could not be saved!");
			e1.printStackTrace();
		}
		return false;
	}

	public static boolean addBlockInfo(Block block, String author, GameMode gameMode) {
		int i = 0;
		for (String objects : Core.getBlocksConfig().getKeys(true)) {
			if ((objects.contains("locations.block")) && (!objects.contains("world")) && (!objects.contains("x"))
					&& (!objects.contains("y")) && (!objects.contains("z"))) {
				i++;
			}
		}

		Core.getBlocksConfig().set("locations.block" + i + ".world", block.getWorld().getName());
		Core.getBlocksConfig().set("locations.block" + i + ".x", Integer.valueOf(block.getX()));
		Core.getBlocksConfig().set("locations.block" + i + ".y", Integer.valueOf(block.getY()));
		Core.getBlocksConfig().set("locations.block" + i + ".z", Integer.valueOf(block.getZ()));
		Core.getBlocksConfig().set("blocks.block" + i + ".author", author);
		Core.getBlocksConfig().set("blocks.block" + i + ".block", block.getType().toString());
		Core.getBlocksConfig().set("blocks.block" + i + ".date", Core.instance.formatDate(System.currentTimeMillis()));
		Core.getBlocksConfig().set("blocks.block" + i + ".gameMode", gameMode.toString());
		Core.getBlocksConfig().set("blocks.block" + i + ".location.world", block.getLocation().getWorld().getName());
		Core.getBlocksConfig().set("blocks.block" + i + ".location.x", Double.valueOf(block.getLocation().getX()));
		Core.getBlocksConfig().set("blocks.block" + i + ".location.y", Double.valueOf(block.getLocation().getY()));
		Core.getBlocksConfig().set("blocks.block" + i + ".location.z", Double.valueOf(block.getLocation().getZ()));
		try {
			Core.getBlocksConfig().save(Core.getBlockFile());
			Core.log("�aBlock file is saved!");
			Core.log("�aBlock added!");
			return true;
		} catch (IOException e1) {
			Core.log("�cBlock file could not be saved!");
			e1.printStackTrace();
		}
		return false;
	}

	public static boolean addBlockInfo(Block block, Player player) {
		int i = 0;
		for (String objects : Core.getBlocksConfig().getKeys(true)) {
			if ((objects.contains("locations.block")) && (!objects.contains("world")) && (!objects.contains("x"))
					&& (!objects.contains("y")) && (!objects.contains("z"))) {
				i++;
			}
		}

		Core.getBlocksConfig().set("locations.block" + i + ".world", block.getWorld().getName());
		Core.getBlocksConfig().set("locations.block" + i + ".x", Integer.valueOf(block.getX()));
		Core.getBlocksConfig().set("locations.block" + i + ".y", Integer.valueOf(block.getY()));
		Core.getBlocksConfig().set("locations.block" + i + ".z", Integer.valueOf(block.getZ()));
		Core.getBlocksConfig().set("blocks.block" + i + ".author", player.getName());
		Core.getBlocksConfig().set("blocks.block" + i + ".block", block.getType().toString());
		Core.getBlocksConfig().set("blocks.block" + i + ".date", Core.instance.formatDate(System.currentTimeMillis()));
		Core.getBlocksConfig().set("blocks.block" + i + ".gameMode", player.getGameMode().toString());
		Core.getBlocksConfig().set("blocks.block" + i + ".location.world", block.getLocation().getWorld().getName());
		Core.getBlocksConfig().set("blocks.block" + i + ".location.x", Double.valueOf(block.getLocation().getX()));
		Core.getBlocksConfig().set("blocks.block" + i + ".location.y", Double.valueOf(block.getLocation().getY()));
		Core.getBlocksConfig().set("blocks.block" + i + ".location.z", Double.valueOf(block.getLocation().getZ()));
		try {
			Core.getBlocksConfig().save(Core.getBlockFile());
			Core.log("�aBlock file is saved!");
			Core.log("�aBlock added!");
			return true;
		} catch (IOException e1) {
			Core.log("�cBlock file could not be saved!");
			e1.printStackTrace();
		}
		return false;
	}
}
