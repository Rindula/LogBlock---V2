package mc.doryan.fr;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import mc.doryan.fr.api.SpigotUpdater;
import mc.doryan.fr.commands.Commands;
import mc.doryan.fr.listeners.Events;
import mc.doryan.fr.objekt.BlockInfo;

public class Core extends JavaPlugin {
	public static Core instance;
	private static YamlConfiguration blocks;
	private SpigotUpdater spigotUpdater;
	private static File blocksFile;
	public ArrayList<Player> isBlockInfo = new ArrayList<>();

	public void onEnable() {
		instance = this;
		try {
			this.spigotUpdater = new SpigotUpdater(this, 32909);
		} catch (IOException e) {
			e.printStackTrace();
		}
		blockInfoYML();

		getConfig().options().copyDefaults(true);
		saveConfig();

		Bukkit.getPluginManager().registerEvents(new Events(), this);
		getCommand("logblock").setExecutor(new Commands());
	}

	public void onDisable() {
		instance = null;
	}

	private void blockInfoYML() {
		blocksFile = new File("plugins/LogBlock/blocks.yml");
		new File("plugins/LogBlock").mkdir();
		if ((!blocksFile.exists()) || (blocksFile == null)) {
			try {
				blocksFile.createNewFile();
				blocks = YamlConfiguration.loadConfiguration(blocksFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			blocks = YamlConfiguration.loadConfiguration(blocksFile);
		}
	}
	
	public void exempleLogBlockAPI() {
		//BLOCK
		Block block = new Location(Bukkit.getWorld("world"), 0, 80, 0).getBlock();	
		
		BlockInfo blockInfo = LogBlockAPI.getBlockinfo(block);	
		Player player = Bukkit.getPlayer("PlayerExemple");
		
		if(blockInfo != null) {
			//AUTHOR PLACE BLOCK
			String author = blockInfo.getAuthor();
			
			//BLOCK TYPE
			String blockType = blockInfo.getBlock();

			//DATE PLACE
			String date = blockInfo.getDate();
			
			//GAMEMODE WITH PLACED
			String gameMode = blockInfo.getGameMode();
			
			//LOCATION BLOCK
			Location location = blockInfo.getLocation();
			
			//SEND MESSAGE INFO
			LogBlockAPI.sendMessageInfoBlock(player, blockInfo);
		} else {
			Core.log("�cThis block is not in the file (this block was placed before inserting the plugin or this block was not placed by a player)!");
		}
		
		//ADDED BLOCK WITH A PLAYER
		LogBlockAPI.addBlockInfo(block, player);
		
		//ADDED BLOCK WITH A INFORMATION
		LogBlockAPI.addBlockInfo(block, "AuthorName", GameMode.CREATIVE);
		
		//REMOVE INFO BLOCK
		LogBlockAPI.removeBlockInfo(block);
		
		//TURN ON DEBUG MESSAGE IN CONSOLE
		LogBlockAPI.CONSOLE_DEBUG = true;
		
		//TURN OFF DEBUG MESSAGE IN CONSOLE
		LogBlockAPI.CONSOLE_DEBUG = true;
	}

	public String formatDate(long seconds) {
		TimeZone tz = TimeZone.getTimeZone(getConfig().getString("hour-data"));
		SimpleDateFormat df = null;
		df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		df.setTimeZone(tz);
		return df.format(new Date(seconds));
	}

	public static YamlConfiguration getBlocksConfig() {
		return blocks;
	}

	public static File getBlockFile() {
		return blocksFile;
	}

	public static void log(String string) {
		LogBlockAPI.log(string);
	}

	public SpigotUpdater getSpigotUpdater() {
		return this.spigotUpdater;
	}
}
