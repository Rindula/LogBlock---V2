package mc.doryan.fr.objekt;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import mc.doryan.fr.Core;

public class BlockInfo {
	
	private String author;
	private String date;
	private String gameMode;
	private String block;
	private Location location;
	private int id;

	public BlockInfo(String blockPath) {
		this.author = Core.getBlocksConfig().getString(blockPath + ".author");
		this.date = Core.getBlocksConfig().getString(blockPath + ".date");
		this.block = Core.getBlocksConfig().getString(blockPath + ".block");
		this.gameMode = Core.getBlocksConfig().getString(blockPath + ".gameMode");
		this.id = Integer.valueOf(blockPath.replace("blocks.block", ""));
		Core.log("BlockConfig="+Core.getBlocksConfig() + " / BlockPath="+blockPath);
		Core.log(Core.getBlocksConfig().getString(blockPath + ".location.world"));
		Core.log(Core.getBlocksConfig()
				.getInt(new StringBuilder(String.valueOf(blockPath)).append(".location.x").toString()) + "");
		Core.log(Core.getBlocksConfig()
				.getInt(new StringBuilder(String.valueOf(blockPath)).append(".location.y").toString()) + "");
		Core.log(Core.getBlocksConfig()
				.getInt(new StringBuilder(String.valueOf(blockPath)).append(".location.z").toString()) + "");
		this.location = new Location(Bukkit.getWorld(Core.getBlocksConfig().getString(blockPath + ".location.world")),
				Core.getBlocksConfig().getInt(blockPath + ".location.x"),
				Core.getBlocksConfig().getInt(blockPath + ".location.y"),
				Core.getBlocksConfig().getInt(blockPath + ".location.z"));
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDate() {
		return this.date;
	}
	
	public int getID() {
		return this.id;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getGameMode() {
		return this.gameMode;
	}

	public void setGameMode(String gameMode) {
		this.gameMode = gameMode;
	}

	public String getBlock() {
		return this.block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
}
